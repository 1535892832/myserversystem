package client;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.SocketException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileSystemView;
import com.sun.awt.AWTUtilities;

import entity.ClientSocket;
import zip.ZipFile;

public class ClientGui extends JFrame {
	private final int port = 8888; // 端口号
	private  String hostUrl = "localhost";// 服务器地址
	private Socket socket = null;
	private BufferedImage img = null;
	private JButton charSystemButton; // 文件系统按钮
	private JPanel systemPanel;
	private Robot robot; // 机器人对象
	private volatile BufferedImage buffImg; // 截屏
	private Dimension dime;
	private Rectangle rect;
	private JTextField inputBox; // 显示输入框
	private JTextArea msgTextBox;// 消息显示
	private JButton sendMsgBut; // 发送消息按钮
	private CharFrame cf;// 聊天系统面板
	private JButton sendFileBut; // 发送文件按钮
	private volatile boolean falg=true; //控制截屏线程
	private JFileChooser jfc; //文件选择控件

	// 输入输出流
	private ObjectOutputStream out;
	private ObjectInputStream in;
	private String saveFileUrl = getHomeUrl() + "clientSaveFile" + File.separator; // 保存文件目录

	public void initSocket() { // 初始化Socket
		try {
			hostUrl=JOptionPane.showInputDialog("请输入服务器ip地址...");
			// 顺便初始化屏幕参数
			robot = new Robot();
			dime = Toolkit.getDefaultToolkit().getScreenSize();
			rect = new Rectangle(dime);
			socket = new Socket(hostUrl, port);
		//	socket.setSoTimeout(6000);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showConfirmDialog(null, "连接服务器失败!!");
			System.exit(0);
		} catch (AWTException e) {
			JOptionPane.showConfirmDialog(null, "机器人初始化失败!!");
			System.exit(0);
		}
	}

	public ClientGui() {
		initSocket();// 初始化Socket
		jfc=new JFileChooser(new File(getHomeUrl())); //初始化文件选择器
		jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES); // 设置可以选择文件夹和目录
		jfc.setMultiSelectionEnabled(true);
		msgTextBox = new JTextArea();
		msgTextBox.setEditable(false); // 不可编辑
		inputBox = new JTextField();
		sendMsgBut = new JButton("发送");
		sendMsgBut.addActionListener(new SendMsgListener()); 
		
		sendFileBut = new JButton("发送文件");
		sendFileBut.addActionListener(new SendFileButListener());
		systemPanel = new JPanel();
		// systemPanel.addMouseListener(new SystemPanelMouseListenenr());
		charSystemButton = new JButton("聊天系统");
		charSystemButton.addMouseListener(new SystemPanelMouseListenenr());
		charSystemButton.addActionListener(new CharSystemListener());
		systemPanel.setBackground(new Color(0, 0, 0, 0));
		systemPanel.add(charSystemButton);
		this.addKeyListener(new GuiKeyListener());
		this.setSize(dime);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().add(systemPanel, BorderLayout.NORTH);
		this.setUndecorated(true); // 允许启用窗口装饰
		AWTUtilities.setWindowOpaque(this, false);// jre lib目录下面的rt.jar 用于透明化窗口
		this.setVisible(true); // 显示

		this.setAlwaysOnTop(true);
		this.setFocusable(true);// 请求焦点
		this.requestFocus();
		initSaveFileDir(); // 初始化保存文件夹
		System.out.println("客户端Gui启动...");
		Thread readThread = new Thread(new ClientThread(this)); //读取数据线程
		readThread.start();
		Thread outThread=new Thread(new ThrowScreen()); //发送截屏线程
		outThread.setDaemon(true); //设置为守护线程
		//outThread.start();
	}
	
	class SendFileButListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			int r = jfc.showOpenDialog(null);
			if (r == JFileChooser.APPROVE_OPTION) { // 如果打开了文件
				File[] files = jfc.getSelectedFiles();
				Thread sendFileAllClient = new Thread(new SendFileAllServer(files));
				sendFileAllClient.start(); // 发送选中文件给客户端线程开始
			}
		}
	}
	
	class SendFileAllServer implements Runnable { // 发送文件给所有客户端
		File[] files;
		public SendFileAllServer(File[] files) {
			this.files = files;
		}

		@Override
		public void run() {
			final int dirhead = 10; // 文件夹头部
			final int fhead = 11; // 文件头部
			for (int i = 0; i < files.length; i++) {
				byte[] data = null;
				int len = 0;// 文件数据文件长度
				if (files[i].isDirectory()) {
					// 如果是文件夹 就传输文件夹
					try {
						data = ZipFile.compressFileDirToBytes(files[i].getAbsolutePath());
						System.out.println("文件:" + files[i].getAbsolutePath() + "压缩完毕!!");
						len = data.length;
					} catch (Exception e1) {
						JOptionPane.showConfirmDialog(null, "文件:" + files[i].getName() + "传输失败");
						e1.printStackTrace();
						continue;
					}
					sendData(dirhead, len, data);
				} else {
					// 传输文件
					try {
						data = ZipFile.compressFileToBytes(files[i]);
						len = data.length;
						sendData(fhead, len, data);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

	}

	class GuiKeyListener extends KeyAdapter {
		/*
		 * 按钮控件的显示于隐藏 (non-Javadoc)
		 * 
		 * @see java.awt.event.KeyAdapter#keyPressed(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyPressed(KeyEvent e) {
			System.out.println(e.getKeyCode());
			if (e.getKeyCode() == 75) { // 如果按下K键
				if (!charSystemButton.isShowing()) {
					charSystemButton.setVisible(true);
				} else {
					charSystemButton.setVisible(false);
				}
			}
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g); // 调用父类方法清除背景
		if (img != null) {
			g.drawImage(img, 0, 0, dime.width, dime.height, null); // 绘制图像
			System.out.println("绘制");
		}
	}

	private class ClientThread implements Runnable { // 循环读取服务器数据
		ClientGui cg;
		public ClientThread(ClientGui cg) {
			this.cg = cg;
		}

		@Override
		public void run() {
			int c = 0;
			int head = -1;
			int len;
			byte[] imgBytes;
			ByteArrayInputStream bin;
			try {
				while ((head = in.readInt()) != -1) {
					switch (head) {
					case 0: // 接受截图
						len = in.readInt();
						imgBytes = new byte[len];
						System.out.println("客户端:读取数据头部->" + head + "\t读取数据长度->" + len);
						// 读取截图
						in.readFully(imgBytes, 0, imgBytes.length);
						bin = new ByteArrayInputStream(imgBytes, 0, imgBytes.length);
//							FileOutputStream fos =new FileOutputStream("C:\\Users\\15358\\Desktop\\screen\\"+c+".jpg");
//							fos.write(imgBytes);
//							fos.flush();
//							fos.close();
						// c++;
						img = ImageIO.read(bin);
						cg.repaint(); // 重绘
						bin.close();
//							Thread showScreen =new Thread(new AcceptScreenThread(bin, cg));
//							showScreen.start();
						break;
					case 1: // 停止接受投屏
						int body = in.readInt();
						img = null;
						cg.repaint(); // 结束投屏
						System.out.println("停止投屏....");
						break;
					case 8:
						int msgLen = in.readInt();
						byte[] bytes = new byte[msgLen];
						in.readFully(bytes, 0, bytes.length);
						String msg = URLDecoder.decode(new String(bytes),"utf-8");
						System.out.println("接收来自服务器的消息-->" + msg);
						msgTextBox.append(msg + "\n");
						break;
					case 10: // 接受服务器发送的文件夹
						int fileFirLen = in.readInt();
						byte[] fileDirBytes = new byte[fileFirLen];
						in.readFully(fileDirBytes, 0, fileDirBytes.length); // 读取数据
						Thread down = new Thread(new DownServerFileDir(fileDirBytes, saveFileUrl));
						down.start();
//							ZipFile.decomFileDirReadBytes(filebytes, saveFileUrl);
						System.out.println("接受文件长度为:" + fileDirBytes.length);
						System.out.println("接受来自服务器的文件夹");
						break;
					case 11:   //读取文件
						int filelen = in.readInt();
						byte[] fileBytes = new byte[filelen];
						in.readFully(fileBytes, 0, fileBytes.length);
						Thread downFile = new Thread(new DownServerFile(fileBytes, saveFileUrl));
						downFile.start();
						System.out.println("接受来自服务器的文件");
						break;
					default:
						break;
					}
				}
			} catch (SocketException se) {
				JOptionPane.showConfirmDialog(null, "服务器断开连接");
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					out.close();
					in.close();
					socket.close();
					System.out.println("客户端安全退出");
					System.exit(0);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	class SendMsgListener implements ActionListener {
		//发送消息给服务器端
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("发送消息");
			String msg =inputBox.getText(); // 获取文本框文本
			if (!msg.equals("")) {
				try {
					msg=URLEncoder.encode(msg, "utf-8"); //编码
					Thread snedMsgThread = new Thread(new SendMsgThead(msg));
					snedMsgThread.start();
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}
	
	class SendMsgThead implements Runnable {
		String msg;
		final int head = 8; // 文件消息头部
		public SendMsgThead(String msg) {
			this.msg = msg;
		}

		@Override
		public void run() {
			System.out.println("客户端发送消息" + msg);
			byte[] bytes = msg.getBytes();
			int len = bytes.length;
			sendData(head, len, bytes);
		}
	}

	class CharSystemListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			if (cf == null || !cf.isShowing()) {
				System.out.println("显示.....");
				cf = new CharFrame();
				cf.setVisible(true);
			}
		}

	}

	class SystemPanelMouseListenenr extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {

		}

		@Override
		public void mouseEntered(MouseEvent e) {
			charSystemButton.setVisible(true);
			System.out.println("鼠标进入控件...");
		}

		@Override
		public void mouseExited(MouseEvent e) {
			charSystemButton.setVisible(false);
			System.out.println("鼠标退出控件....");
		}
	}

	class DownServerFile implements Runnable {
		// 多线程接收文件
		byte[] data;
		String saveUrl;

		public DownServerFile(byte[] data, String saveUrl) {
			this.data = data;
			this.saveUrl = saveUrl;
		}

		// 接收来自服务器的文件夹
		@Override
		public void run() {
			ZipFile.decomFileReadBytes(data, saveUrl);
		}
	}

	class CharFrame extends JFrame {
		public CharFrame() {
			JScrollPane jsp = new JScrollPane(msgTextBox);
			jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			this.getContentPane().add(jsp, BorderLayout.CENTER);
			JPanel southPane = new JPanel(new BorderLayout());
			southPane.add(inputBox, BorderLayout.CENTER);
			southPane.add(sendMsgBut, BorderLayout.EAST);
			southPane.add(sendFileBut, BorderLayout.SOUTH);
			this.getContentPane().add(southPane, BorderLayout.SOUTH);
			this.setTitle("聊天系统");
			this.setLocationRelativeTo(null);
			this.setSize(500, 300);
		}
	}

	class DownServerFileDir implements Runnable {
		byte[] data;
		String saveUrl;

		public DownServerFileDir(byte[] data, String saveUrl) {
			this.data = data;
			this.saveUrl = saveUrl;
		}

		// 接收来自服务器的文件夹
		@Override
		public void run() {
			ZipFile.decomFileDirReadBytes(data, saveUrl);
		}
	}

	class AcceptScreenThread implements Runnable {
		// 显示截屏线程
		ByteArrayInputStream bin;
		ClientGui cg;

		// 接受并显示图片线程
		public AcceptScreenThread(ByteArrayInputStream bin, ClientGui cg) {
			this.bin = bin;
			this.cg = cg;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				img = ImageIO.read(bin);
				cg.repaint(); // 重绘
				System.out.println("客户端重绘...");
				bin.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void sendData(int head, int body, byte[] bytes) { // 发送数据
		synchronized (out) {
			try {
				out.writeInt(head);
				out.writeInt(body);
				out.write(bytes, 0, bytes.length);
				out.flush();
			} catch (IOException e) {
				System.out.println("发送数据异常...");
			}
		}
	}

	class ThrowScreen implements Runnable { // 像客户端发送截图
		@Override
		public void run() {
			while (falg) {
				sendScreenCap();
		}
	}
	}
		
	private void sendScreenCap() {     //客户端截屏发送服务端
		/*
		 * head = 0 len =数据长度 imgbyte=数据长度 把截屏发给各个客户端 协议部分 截图协议 head 0 写入数据长度 写入数据
		 */
		int head = 0;// 头部
		ByteArrayOutputStream bos = null;
		// 截屏
		buffImg = robot.createScreenCapture(rect); // 截屏
		try {
			bos = new ByteArrayOutputStream();
			ImageIO.write(buffImg, "jpg", bos);
			byte[] imgBytes = bos.toByteArray();// 获取截图bytes
			int len = imgBytes.length;
			bos.close();// 关闭资源
			sendData(head, len, imgBytes); // 发送截图
        
			//System.out.println("发送截屏给服务器");
		} catch (IOException e) {
			// System.out.println("客户端异常");

		}
	}

	private void initSaveFileDir() {
		// 初始化保存文件夹
		File saveFile = new File(saveFileUrl);
		if (!saveFile.exists()) {
			saveFile.mkdirs(); // 创建文件夹
			System.out.println("初始化文件夹成功!!");
		}
	}

	private String getHomeUrl() { // 获取计算机桌面目录
		FileSystemView fsv = FileSystemView.getFileSystemView();
		File home = fsv.getHomeDirectory();
		String homeUrl = home.getAbsolutePath() + File.separator;
		return homeUrl;
	}

	public static void main(String[] args) {
		new ClientGui();
	}
}
