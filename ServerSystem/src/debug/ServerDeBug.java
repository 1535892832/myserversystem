package debug;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class ServerDeBug {
	/*
	 * 找到原因不能用getScreentoByte方法
	 */
	
	
	private final int port = 8888;
	Robot robot;
	ServerSocket server;
	Socket socket;
	boolean falg = false;
	ArrayList<ObjectOutputStream> clientOuts = new ArrayList<ObjectOutputStream>();
	Rectangle rect;

	public ServerDeBug() {
		try {
			robot = new Robot();
			rect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
			server = new ServerSocket(port);
			System.out.println("服务器启动...");
//			Thread thread =new Thread(new ThrowScreen());
//			thread.start();
			while (true) {
				socket = server.accept();
				System.out.println("用户连接成功...");
				Thread thread = new Thread(new ServerThread(socket));
				thread.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				server.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	class ServerThread implements Runnable {
		boolean tfalg = true;
		Socket socket;
		ObjectOutputStream out;

		public ServerThread(Socket socket) {
			this.socket = socket;
			try {
				out = new ObjectOutputStream(socket.getOutputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			try {
			int type=0;
			
			while (tfalg) {
				BufferedImage img = robot.createScreenCapture(rect);
				ByteArrayOutputStream bao = new ByteArrayOutputStream();
				ImageIO.write(img, "jpg", bao);
				 byte[] imgbyte=bao.toByteArray();
					System.out.println(imgbyte.length);
					out.writeInt(type);
					out.writeInt(imgbyte.length);
					out.write(imgbyte, 0, imgbyte.length);
			}
				} catch (IOException e) {
					try {
						this.socket.close();
						tfalg = false;
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				
			}
			System.out.println("用户安全退出");
		}
	}

	/*
	 * class ThrowScreen implements Runnable {
	 * 
	 * @Override public void run() { falg = true; try { while (falg) { if
	 * (!clientOuts.isEmpty()&&clientOuts.size()>0) { System.out.println("服务器截屏");
	 * for (ObjectOutputStream out : clientOuts) { byte[] imgByte =
	 * getScreenBytes(); int type=0; int len=imgByte.length; out.writeObject(type);
	 * System.out.println("写入数据长度-->"+imgByte.length); out.write(imgByte, 0,
	 * imgByte.length); out.flush(); } }
	 * System.out.println("服务器人数-->"+clientOuts.size()); } } catch (IOException e) {
	 * // TODO Auto-generated catch block e.printStackTrace(); } } }
	 */

	// 获取屏幕截图转byte
	private byte[] getScreenBytes() throws IOException {
		BufferedImage img = robot.createScreenCapture(rect);
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		ImageIO.write(img, "jpg", bao);
		return bao.toByteArray();
	}

	public static void main(String[] args) {
		new ServerDeBug();
	}
}
