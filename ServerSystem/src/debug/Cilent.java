package debug;

import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class Cilent extends JFrame{

	Socket socket;
	private final int port = 8888;
	ObjectOutputStream oos;
	ObjectInputStream oin;
	BufferedImage img=null;
	public Cilent() {
	  try {
		socket=new Socket("localhost", port);
		oos=new ObjectOutputStream(socket.getOutputStream());
		oin=new ObjectInputStream(socket.getInputStream());
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  Thread thread =new Thread(new ClientThread(this));
	  thread.start();
	  this.setSize(Toolkit.getDefaultToolkit().getScreenSize());
	  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  this.setVisible(true);
	}
	
	class ClientThread implements Runnable{
		
		Cilent client;
		
		public ClientThread(Cilent client) {
		   this.client=client;
		}
		
		@Override
		public void run() {
			int type=-1;
			ByteArrayInputStream bin;
			try {
				System.out.println("线程运行");
				while((type=oin.readInt())!=-1) {
			            System.out.println("type-->"+type);
			             int len=oin.readInt();
			           System.out.println("长度-->"+len);
			           byte[] imgbyte=new byte[len];
			           oin.readFully(imgbyte, 0, imgbyte.length);
			            bin=new ByteArrayInputStream(imgbyte);
			            img =ImageIO.read(bin);
			            client.repaint();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
	}
	
	
	@Override
	public void paint(Graphics g) {
		  super.paint(g);
		if(img!=null) {
			System.out.println("绘制图片");
			g.drawImage(img, 0, 0, this.getWidth(),this.getHeight(), null);
		}
	}
	
	public static void main(String[] args) {
		new Cilent();
	}
}
