package gui;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Vector;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileSystemView;
import entity.ClientSocket;
import mypanel.TitleLable;
import zip.ZipFile;

/*
 * 明天写文件系统 顺便把网页美工实操作业写了
 * 
 * 准备实现客户端 服务器的互通操作
 * 
 * 
 */

public class Gui extends JFrame {
	/*
	 * 服务器端Gui
	 */
	private final int port = 8888; // 端口号
	private JPanel menuPane;// 菜单Pane
	private CardLayout cardLayout; // 用于变换容器
	private JMenu mainPage;// 主页面菜单
	private JMenu charMenu;// 聊天菜单
	private JMenu screenMenu;// 投屏菜单
	private JMenu fileMenu; // 文件系统
	private JCheckBox isAllCheak;// 是否发送全部文件
	private JMenuItem start;// 开始投屏
	private JMenuItem stop;// 停止投屏
	private JMenu serverMenu; // 服务器设置
	private JMenu lookMenu; // 查看客户端
	private JPanel mainPanel;// 中间主页面
	private JPanel mainLabel;// 主页面显示
	private JPanel serverLabel;// 服务器设置容器
	private JPanel charLabel; // 聊天面板容器
	private JTextArea charArea;// 聊天文本区
	private JTextField textBox;
	private JButton sendMsgBtn; // 发送消息按钮
	private JPanel fileLabel; // 文件系统标签
	private JPanel lookLabel; // 查看客户端容器
	private JList clientList;
	private ServerSocket server = null;// 服务器
	private Socket socket = null;
	private Vector<ClientSocket> clients = new Vector<ClientSocket>(); // 客户端对象
	private Robot robot; // 机器人对象
	private Rectangle rect; // 矩形 屏幕大小
	private JPanel showPane; // 显示容器
	private volatile BufferedImage buffImg = null; // 用于缓存截屏
	private BufferedImage showImage=null;
	private BufferedImage img = null;// 用于显示
	private volatile boolean falg = false;// 控制是否投屏 加入关键字volatile控制线程
	private Thread screenThread = null; // 发送截图数据线程
	private Thread sendFileAllClient = null; // 发送文件线程
	private JButton sendFileButton; // 发送文件按钮
	private int c = 0;
	private String fileSaveUrl = getHomeUrl() + "ClientDir" + File.separator; // 默认文件夹为桌面的clientDir
	private JFileChooser jfc; //文件选择控件

	public Gui() { // 初始化
		initGui();
		Thread showPaneThread =new Thread(new ShowScreenThread());
		showPaneThread.start();   //显示客户端图像线程
		this.setTitle("服务器端");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1500, 1000);
		this.setLocation(getScreenCenter());
		this.setVisible(true);
		initDir(fileSaveUrl); // 初始化文件存放位置
		initServer();// 初始化服务器
	}

	private void initGui() { // 初始化Gui页面

		menuPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JMenuBar menuBar = new JMenuBar();// 菜单容器
		mainPage = new JMenu("主页面");
		fileMenu = new JMenu("文件系统");
		charMenu = new JMenu("聊天系统");
		charMenu.addMouseListener(new CharMenuListener());
		screenMenu = new JMenu("投屏系统");
		serverMenu = new JMenu("服务器设置");
		lookMenu = new JMenu("查看客户端");
		cardLayout = new CardLayout();
		mainPanel = new JPanel(cardLayout);
		Font menuFont = new Font("楷体", Font.BOLD, 25);// 设置笔的样式
		Font menuItemFont = new Font("华文中宋", Font.ITALIC, 25);
		// 实例化容器页面
		mainLabel = new JPanel(new BorderLayout());
		JLabel mainTitle = new TitleLable("主页面");
		mainLabel.add(mainTitle, BorderLayout.NORTH);
		mainPage.addMouseListener(new MainPageActionListener());

		serverLabel = new JPanel(new BorderLayout());
		TitleLable serverTitle = new TitleLable("服务器设置");
		serverLabel.add(serverTitle, BorderLayout.NORTH);
		serverMenu.addMouseListener(new ServerMenuListener());

		// 文件系统容器
//		fileLabel=new JPanel(new BorderLayout());
//		TitleLable fileTitle =new TitleLable("文件系统");
//		JPanel bpanel =new JPanel(); //文件系统按钮容器
//		sendFileButton =new JButton("发送文件");
//		bpanel.add(sendFileButton);
//		fileLabel.add(fileTitle, BorderLayout.NORTH);
//		fileLabel.add(bpanel,BorderLayout.CENTER);

		// 聊天系统容器
		charLabel = new JPanel(new BorderLayout());
		TitleLable charTitle = new TitleLable("聊天系统");
		charArea = new JTextArea(5, 5);
		// charArea.setEditable(false);
		charArea.setFont(new Font("微软雅黑", Font.BOLD, 24));
		JScrollPane jsp=new JScrollPane(charArea);
		jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER); //禁用水平滚动条
		textBox = new JTextField(40);
		textBox.setFont(new Font("华文楷体", Font.BOLD, 20));
		textBox.setForeground(Color.RED);
		sendMsgBtn = new JButton("发送");
		sendMsgBtn.addActionListener(new SendMsgListener());
		sendMsgBtn.setFont(new Font("华文行楷", Font.BOLD, 20));
		JPanel inputPanel = new JPanel(new BorderLayout());
		inputPanel.add(textBox, BorderLayout.CENTER);
		inputPanel.add(sendMsgBtn, BorderLayout.SOUTH);
		charLabel.add(charTitle, BorderLayout.NORTH);
		charLabel.add(jsp, BorderLayout.CENTER);
		charLabel.add(inputPanel, BorderLayout.SOUTH);

		// 查看面板容器
		lookLabel = new JPanel(new BorderLayout());
		clientList = new JList(clients);
		sendFileButton = new JButton("发送文件");
		sendFileButton.addActionListener(new SendFileButtonListener());
		sendFileButton.setFont(new Font("隶体", Font.BOLD, 20));
		isAllCheak = new JCheckBox("发送给全部客户端");
		isAllCheak.setFont(new Font("篆体", Font.BOLD, 20));
		isAllCheak.setSelected(true); // 默认为选中
		JPanel bpanel = new JPanel(new BorderLayout());
		bpanel.add(isAllCheak, BorderLayout.NORTH);
		bpanel.add(sendFileButton, BorderLayout.CENTER);
		clientList.setFont(new Font("楷体", Font.BOLD, 25));
		clientList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION); // 设置多选
		clientList.addListSelectionListener(new JListChangeListener());
		JPanel westPane = new JPanel(new BorderLayout());
		JScrollPane scrollPane = new JScrollPane(clientList);
		JLabel hint = new JLabel("客户端在线人数-->  " + clients.size() + "人");
		hint.setBackground(Color.WHITE);
		hint.setOpaque(true);
		hint.setFont(new Font("宋体", Font.TRUETYPE_FONT, 20));
		hint.setForeground(Color.MAGENTA);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER); // 禁用水平滚动条
		westPane.add(hint, BorderLayout.NORTH);
		westPane.add(scrollPane, BorderLayout.CENTER);
		westPane.add(bpanel, BorderLayout.SOUTH);
		showPane = new ShowPane(this); // 显示面板
		lookLabel.add(westPane, BorderLayout.WEST);
		lookLabel.add(showPane, BorderLayout.CENTER);// 将显示区域添加到面板中间
		lookMenu.addMouseListener(new LookMenuListener());

		// 添加菜单值
		start = new JMenuItem("开始投屏");
		stop = new JMenuItem("停止投屏");
		screenMenu.add(start);
		screenMenu.add(stop);
		start.addActionListener(new StartListener());
		stop.addActionListener(new StopListener());

		// 添加到card布局
		mainPanel.add(serverLabel, "serverLabel");
		mainPanel.add(mainLabel, "mainLabel");
		mainPanel.add(lookLabel, "lookLabel");
		mainPanel.add(charLabel, "charLabel");

		// 设置菜单画笔
		mainPage.setFont(menuFont);
		fileMenu.setFont(menuFont);
		charMenu.setFont(menuFont);
		screenMenu.setFont(menuFont);
		serverMenu.setFont(menuFont);
		lookMenu.setFont(menuFont);
		start.setFont(menuItemFont);
		stop.setFont(menuItemFont);
		start.setForeground(Color.GREEN);
		stop.setForeground(Color.RED);

		// 添加菜单
		menuBar.add(mainPage);
		menuBar.add(fileMenu);
		menuBar.add(charMenu);
		menuBar.add(screenMenu);
		menuBar.add(serverMenu);
		menuBar.add(lookMenu);
		menuPane.add(menuBar);
		this.getContentPane().add(menuPane, BorderLayout.NORTH);// 将菜单放置北区
		this.getContentPane().add(mainPanel, BorderLayout.CENTER);
		cardLayout.show(mainPanel, "mainLabel"); // 切换卡片
	}

	private void initServer() {
		try {
			robot = new Robot();
			rect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
			server = new ServerSocket(port);
			System.out.println("服务器启动成功!!!");
			while (true) { // 循环等待客户端连接
				socket = server.accept();
				ClientSocket cs = new ClientSocket(socket);// 包装成用户类
				clients.add(cs);
 			 Thread readThread =new Thread(new ClientReadThread(cs)); //读取客户端线程 
			 readThread.start();
				System.out.println("当前客户端人数-->" + clients.size());
			}
		} catch (BindException e) {
			JOptionPane.showConfirmDialog(null, "服务器已经启动...");
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			JOptionPane.showConfirmDialog(null, "服务器启动失败...");
			e.printStackTrace();
			System.exit(0);
		} catch (AWTException e) {
			JOptionPane.showConfirmDialog(null, "机器人初始化失败...");
			System.exit(0);
		}
	}

	class ClientReadThread implements Runnable {
		ClientSocket cs;
		ObjectInputStream cin;

		public ClientReadThread(ClientSocket cs) {
			this.cs = cs;
			cin = this.cs.getCin(); // 获取输入流
		}

		@Override
		public void run() {
			System.out.println("开始监听"+cs.getClientName());
			int c = 0;
			int head = -1;
			int len;
			byte[] imgBytes;
			ByteArrayInputStream bin = null;
			try {
				while ((head = cin.readInt()) != -1) {
					switch (head) {
					case 0: // 接受截图
						len = cin.readInt();
						imgBytes = new byte[len];
					//	System.out.println("客户端:读取数据头部->" + head + "\t读取数据长度->" + len);
						// 读取截图
						cin.readFully(imgBytes, 0, imgBytes.length);
						bin=new ByteArrayInputStream(imgBytes, 0, imgBytes.length);
						img = ImageIO.read(bin);
						cs.setImg(img); // 将图片赋予自身对象
						cs.setRefresh(true); //设置图片刷新
						bin.close();
					//	System.out.println("接收来自客户端的截图..");
						break;
					case 8:
						int msgLen = cin.readInt();
						byte[] bytes = new byte[msgLen];
						cin.readFully(bytes, 0, bytes.length);
						String msg = URLDecoder.decode(new String(bytes),"utf-8");
						System.out.println("接收来自客户端的消息-->" + msg);
						charArea.append(msg + "\n");
						msg=URLEncoder.encode(msg, "utf-8");
						//把消息发给全部客户端
						Thread snedMsgThread = new Thread(new SendMsgThead(msg));
						snedMsgThread.start();
						break;
					case 10: // 接受服务器发送的文件夹
						int fileFirLen = cin.readInt();
						byte[] fileDirBytes = new byte[fileFirLen];
						cin.readFully(fileDirBytes, 0, fileDirBytes.length); // 读取数据
						Thread down = new Thread(new DownClientFile(fileDirBytes, fileSaveUrl));
						down.start();
//							ZipFile.decomFileDirReadBytes(filebytes, saveFileUrl);
						System.out.println("接受文件长度为:" + fileDirBytes.length);
						System.out.println("接受来自服务器的文件夹");
						break;
					case 11:   //读取文件
						int filelen = cin.readInt();
						byte[] fileBytes = new byte[filelen];
						cin.readFully(fileBytes, 0, fileBytes.length);
						Thread downFile = new Thread(new DownClientFile(fileBytes, fileSaveUrl));
						downFile.start();
						System.out.println("接受来自服务器的文件");
						break;
					default:
						break;
					}
				}
			} catch (SocketException se) {
				JOptionPane.showConfirmDialog(null, "服务器断开连接");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	class DownClientFile implements Runnable {
		byte[] data;
		String saveUrl;

		public DownClientFile(byte[] data, String saveUrl) {
			this.data = data;
			this.saveUrl = saveUrl;
		}

		// 接收来自服务器的文件夹
		@Override
		public void run() {
			ZipFile.decomFileDirReadBytes(data, saveUrl);
		}
	}
	
	class ShowScreenThread implements Runnable{
		//循环显示客户端内容
		@Override
		public void run() {
		   while(true) {
			   int[] indexs=clientList.getSelectedIndices();
			   System.out.println("一共选择-->"+indexs.length);
			   if(indexs.length>0) {
				//   System.out.println("第一个索引--> "+indexs[0]);
				   if(clients.get(indexs[0]).getImg()!=null) {  //如果选中的客户端对象图片不为空
					   showImage=clients.get(indexs[0]).getImg();
					   showPane.repaint();
					   System.out.println("ShowPane重绘..");
				   }
			   }
		   }
		}
	}
	
	class CharMenuListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			cardLayout.show(mainPanel, "charLabel");
		}
	}

	class SendFileButtonListener implements ActionListener { // 发送文件按钮
		/*
		 * 文件夹协议为 head 10 body 数据长度 data=数据
		 * 
		 */

		@Override
		public void actionPerformed(ActionEvent e) {
			// 如果客户端为空 返回
			if (clients.isEmpty() || clients.size() == 0) {
				JOptionPane.showConfirmDialog(null, "客户端列表为空!", "空列表", JOptionPane.CLOSED_OPTION,
						JOptionPane.PLAIN_MESSAGE);
				return;
			}
			int r = jfc.showOpenDialog(null);
			if (r == JFileChooser.APPROVE_OPTION) { // 如果打开了文件
				File[] files = jfc.getSelectedFiles();
				// 如果没有选择就全部发送否则
				if (isAllCheak.isSelected()) {
//					if(falg) {
//						falg=false; //暂时停止线程
//						try {
//							screenThread.join();
//						} catch (InterruptedException e2) {
//							// TODO Auto-generated catch block
//							e2.printStackTrace();
//						}
//						System.out.println("停止截图...");
					sendFileAllClient = new Thread(new SendFileAllClient(files));
					sendFileAllClient.start(); // 发送选中文件给客户端线程开始
//					try {
//						sendFileAllClient.join();
//					} catch (InterruptedException e1) {
//						// TODO Auto-generated catch block
//						e1.printStackTrace();
//					}
//					falg=true;
//					screenThread=new Thread(new ThrowScreen());
//					screenThread.start();
//					System.out.println("继续截图...");
					System.out.println("发送文件给所有客户端");
				} else {
					sendFileAllClient = new Thread(new SendFileAllClient(files));
					sendFileAllClient.start(); // 发送选中文件给客户端线程开始
					try {
						sendFileAllClient.join();
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			} else {
				System.out.println("发送部分客户端!!!");
				int[] selectIndexs = clientList.getSelectedIndices(); // 获取选择索引
				for (int i : selectIndexs) {
					System.out.println("发送给" + i + "号用户\t用户名-->" + clients.get(i).getClientName());
				}
			}
//				System.out.println("发送文件");
		}
	}

	class SendFileAllClient implements Runnable { // 发送文件给所有客户端
		File[] files;

		public SendFileAllClient(File[] files) {
			this.files = files;
		}

		@Override
		public void run() {
			final int dirhead = 10; // 文件夹头部
			final int fhead = 11; // 文件头部
			ClientSocket cs = null;
			ObjectOutputStream cout = null;
			Iterator<ClientSocket> it = null;
			for (int i = 0; i < files.length; i++) {
				it = clients.iterator();
				byte[] data = null;
				int len = 0;// 文件数据文件长度
				if (files[i].isDirectory()) {
					// 如果是文件夹 就传输文件夹
					try {
						data = ZipFile.compressFileDirToBytes(files[i].getAbsolutePath());
						System.out.println("文件:" + files[i].getAbsolutePath() + "压缩完毕!!");
						len = data.length;
					} catch (Exception e1) {
						JOptionPane.showConfirmDialog(null, "文件:" + files[i].getName() + "传输失败");
						e1.printStackTrace();
						continue;
					}
					sendData(dirhead, len, data);
				} else {
					// 传输文件
					try {
						data = ZipFile.compressFileToBytes(files[i]);
						len = data.length;
						sendData(fhead, len, data);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

	}

	private void sendData(int head, int body, byte[] bytes) { // 发送数据
		ClientSocket cs = null;
		ObjectOutputStream cout = null;
		Iterator<ClientSocket> it = clients.iterator();
		while (it.hasNext()) {
			cs = it.next();
			synchronized (cs) {
				cout = cs.getCout();
				try {
					cout.writeInt(head);
					cout.writeInt(body);
					cout.write(bytes, 0, bytes.length);
					cout.flush();
				} catch (IOException e) {
					try {
						if (cs != null) {
							cs.getSocket().close();
							clients.remove(cs); // 从客户端列表移除
							refreshList();
							System.out.println("发送异常 移除客户端!!!");
							System.out.println("客户端人数-->" + clients.size());
						}
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		}
	}

	private void refreshList() {
		// 刷新客户端列表
		clientList.setListData(clients);
	}

	class StartListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("开始投屏按钮按下");
			if(!falg) {
			falg = true;
			screenThread = new Thread(new ThrowScreen());
			screenThread.start();
			System.out.println("开始向客户端发送截屏数据");
			}
		}
	}

	// 发送消息按钮
	class SendMsgListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("发送消息");
			String msg =textBox.getText(); // 获取文本框文本
			if (!msg.equals("")) {
				try {
					charArea.append(msg+"\n");
					msg=URLEncoder.encode(msg, "utf-8"); //编码
					Thread snedMsgThread = new Thread(new SendMsgThead(msg));
					snedMsgThread.start();
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}

	
	
	class SendMsgThead implements Runnable {
		String msg;
		final int head = 8; // 文件消息头部

		public SendMsgThead(String msg) {
			this.msg = msg;
		}

		@Override
		public void run() {
			System.out.println("服务器发送消息" + msg);
			byte[] bytes = msg.getBytes();
			int len = bytes.length;
			sendData(head, len, bytes);
		}

	}

	class ThrowScreen implements Runnable { // 像客户端发送截图
		@Override
		public void run() {
			while (falg) {
				sendScreenCap();
			}
			System.out.println("结束投屏");
		}
	}

	class StopListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("停止投屏按钮按下");
			if (falg) { // 结束投屏
				falg = false;
				if (screenThread.isAlive()) {
					try {
						screenThread.join();// 等待截屏线程执行完毕
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				// 向客户端发送结束投屏的指令
				Thread shopSendScreenThread = new Thread(new StopSendScreenThread());
				shopSendScreenThread.start();
			}
		}
	}

	class JListChangeListener implements ListSelectionListener { // List选择改变事件
		@Override
		public void valueChanged(ListSelectionEvent e) {
			JList list = (JList) e.getSource();
			list.setSelectionForeground(Color.CYAN); // 设置选中的值的颜色为淡蓝色\
			
		}
	}

	class LookMenuListener extends MouseAdapter { // 客户端菜单点击
		@Override
		public void mouseClicked(MouseEvent e) {
			cardLayout.show(mainPanel, "lookLabel"); // 切换卡片
			clientList.setListData(clients);
		}
	}

	class ServerMenuListener extends MouseAdapter {// 服务器设置菜单

		@Override
		public void mouseClicked(MouseEvent e) {
			cardLayout.show(mainPanel, "serverLabel"); // 切换卡片
		}
	}

	class MainPageActionListener extends MouseAdapter { // 点击主页面菜单
		@Override
		public void mouseClicked(MouseEvent e) {
			cardLayout.show(mainPanel, "mainLabel"); // 切换卡片
		}
	}

	public void drawImage(Graphics g) { // 绘制面板
		if (showImage != null) {
			g.clearRect(0, 0, showPane.getWidth(), showPane.getHeight()); // 清除面板
			g.drawImage(showImage, 0, 0, showPane.getWidth(), showPane.getHeight(), null);
			System.out.println("绘制面板");
		} else {
			if (clients.isEmpty() && clients.size() == 0) {
				int fontSize = 40;
				g.setFont(new Font("宋体", Font.BOLD, fontSize));
				g.setColor(Color.RED);
				g.drawString("暂无客户端", (showPane.getWidth() / 2 - fontSize - 20), (showPane.getHeight() / 2 - fontSize));
			}
		}
	}

	class StopSendScreenThread implements Runnable {
		@Override
		public void run() {
			stopSendScreenCap();
		}
	}

	private void stopSendScreenCap() { // 停止截屏
		/*
		 * head=1 body=0 0暂时代表关闭
		 */
		if (!clients.isEmpty() && clients.size() > 0) {
			int head = 1;
			int body = 0;
			Iterator<ClientSocket> it = clients.iterator();
			ObjectOutputStream cout = null;
			ClientSocket mcs = null;
			// 循环写入数据
			while (it.hasNext()) {
				try {
					mcs = it.next();
					cout = mcs.getCout();
					cout.writeInt(head);
					cout.writeInt(body);
					cout.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private void sendScreenCap() {
		/*
		 * head = 0 len =数据长度 imgbyte=数据长度 把截屏发给各个客户端 协议部分 截图协议 head 0 写入数据长度 写入数据
		 */
		if (!clients.isEmpty() && clients.size() > 0) {
			int head = 0;// 头部
			Iterator<ClientSocket> it = clients.iterator();
			// 遍历所有客户端对象
			ClientSocket mcs = null;
			ObjectOutputStream cout = null;
			ByteArrayOutputStream bos = null;
			// 截屏
			buffImg = robot.createScreenCapture(rect); // 截屏
//			ImageIO.write(buffImg, "jpg",
//					new FileOutputStream("C:\\Users\\15358\\Desktop\\serverScreen\\" + c + ".jpg"));
//			c++;
			try {
				bos = new ByteArrayOutputStream();
				ImageIO.write(buffImg, "jpg", bos);
				byte[] imgBytes = bos.toByteArray();// 获取截图bytes
				int len = imgBytes.length;
				bos.close();// 关闭资源
				sendData(head, len, imgBytes); // 发送截图

			} catch (IOException e) {
				// System.out.println("客户端异常");

			}
		}
	}

	/*
	 * class SendOnlyDataThread implements Runnable{
	 * 
	 * // 使用多线程传输截图
	 * 
	 * int head; int body; byte[] bytes; ObjectOutputStream oos; ClientSocket mcs;
	 * public SendOnlyDataThread(int head,int body,byte[] bytes,ClientSocket mcs) {
	 * this.head=head; this.body=body; this.bytes=bytes; this.mcs=mcs;
	 * this.oos=mcs.getCout(); }
	 * 
	 * @Override public void run() { try { oos.writeInt(this.head);
	 * oos.writeInt(this.body); oos.write(bytes, 0, bytes.length); oos.flush(); }
	 * catch (IOException e) { try { if(mcs!=null) { mcs.getSocket().close();
	 * clients.remove(mcs); //从客户端列表移除 System.out.println("发送异常 移除客户端!!!");
	 * System.out.println("客户端人数-->"+clients.size()); } } catch (IOException e1) {
	 * // TODO Auto-generated catch block e1.printStackTrace(); } } } }
	 */

	private String getHomeUrl() { // 获取计算机桌面目录
		FileSystemView fsv = FileSystemView.getFileSystemView();
		File home = fsv.getHomeDirectory();
		String homeUrl = home.getAbsolutePath() + File.separator;
		return homeUrl;
	}

	private void initDir(String url) {
		// 初始化桌面文件夹
		File file = new File(url);
		jfc = new JFileChooser(file); // 初始化文件选择器
		jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES); // 设置可以选择文件夹和目录
		jfc.setMultiSelectionEnabled(true);
		if (!file.exists()) {
			file.mkdirs();
		}
		System.out.println("初始化桌面文件夹成功!!!");
	}

	private Point getScreenCenter() { // 计算使窗口出现在屏幕中间

		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int deviceWidth = screen.width;
		int deviceHeight = screen.height;

		int width = this.getWidth();
		int height = this.getHeight();

		int centerX = (deviceWidth / 2) - (width / 2);
		int centerY = (deviceHeight / 2) - (height / 2);
		return new Point(centerX, centerY);
	}

	public static void main(String[] args) {
		new Gui();
	}
}
