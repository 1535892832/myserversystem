package gui;

import java.awt.Graphics;

import javax.swing.JPanel;

public class ShowPane extends JPanel{
     Gui gui;
	public ShowPane(Gui gui) {
		this.gui=gui;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
	     gui.drawImage(g);
	}
	
}
