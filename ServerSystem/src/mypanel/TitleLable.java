package mypanel;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;

public class TitleLable extends JLabel{

	public TitleLable(String title) {
		super(title,JLabel.CENTER);
		Font titleFont =new Font("宋体", Font.BOLD, 22);//设置笔的样式
		this.setFont(titleFont);
		this.setBackground(Color.PINK);
		this.setOpaque(true);//立即变色
	}
	
}
