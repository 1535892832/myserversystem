package entity;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class ClientSocket {
/*
 * 客户端实体类
 */
	private  Socket socket;
	private String clientName; //客户端名称
	private ObjectOutputStream cout;//客户端输出流
	private ObjectInputStream cin; //客户端输入流
	private InetAddress ip;
	private BufferedImage img;
	private volatile boolean refresh;
	
	public ClientSocket(Socket socket) {
	 this.socket=socket;
	 this.ip=socket.getInetAddress();
	 this.clientName=ip.getHostAddress();
	 try {
		cout=new ObjectOutputStream(socket.getOutputStream());
		cin=new ObjectInputStream(socket.getInputStream());
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	
	public Socket getSocket() {
		return socket;
	}
	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public ObjectOutputStream getCout() {
		return cout;
	}
	public void setCout(ObjectOutputStream cout) {
		this.cout = cout;
	}
	public InetAddress getIp() {
		return ip;
	}
	public void setIp(InetAddress ip) {
		this.ip = ip;
	}
	
	public boolean isRefresh() {
			return refresh;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	public BufferedImage getImg() {
	
			return img;
		
	}

	public void setImg(BufferedImage img) {
		synchronized (img) {
			this.img = img;
		}
	}
	
	public ObjectInputStream getCin() {
		return cin;
	}

	public void setCin(ObjectInputStream cin) {
		this.cin = cin;
	}

	@Override
		public String toString() { //重写toString方法 Jlist转换列表会调用此方法
		return this.clientName;
		}
	
}
